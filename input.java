public class input {
    
    private int wert;
    private int interval;

    public input(int s, int i){
        wert = s;
        interval = i;
    }

    public int get_interval(){
        return interval;
    }

    public int gen_next_input(){
        wert+=interval;
        return wert;
    }

    public int mult_input(boolean u, int u_b, boolean l, int l_b){
        if(u && l){
            wert = (u_b+l_b)/2;
        }else if(u){
            wert/=2;
        }
        else{
            wert*=10;
        }
        return wert;
    }

}
