public class problem {
    
    String name;

    public problem(String n){
        name = n;
    }

    public boolean c(String[] v_r, String[] v_sr){
        switch (name) {
            case "part":
                for(int i=0;i<v_r.length;i++){
                    if(Integer.parseInt(v_sr[0])-Integer.parseInt(v_r[0])>0){
                        return false;
                    }

                }
                return true;
            default:
                for(int i=0;i<v_r.length;i++){
                    if(!(v_r[i].equals(v_sr[i]))){
                        return false;
                    }
                }
            return true;
        }  
    }

    public String m(String in_str){
        switch (name) {
            case "part":
                int g1=0;
                int g2=0;

                String s1="";
                String s2="";

                for (String line : in_str.split((" "))) {
                    if(line.contains("s1")){
                        g1 = g1 + Integer.parseInt(line.split(":")[1]);
                        s1+=" "+Integer.parseInt(line.split(":")[1]);
                    }
                    else{
                        g2 = g2 + Integer.parseInt(line.split(":")[1]);
                        s2+=" "+Integer.parseInt(line.split(":")[1]);
                    }
                }

                return String.valueOf(Math.abs(g1-g2))+" p1:"+s1+" p2"+s2;
        
            default:
                return in_str;
        }
    }

}
