import java.util.Collections;
import java.util.LinkedList;
import java.util.Random;

public class generator {

    public static void main(String[] args) {
        
        String line = args[0];

        int n = Integer.parseInt(line);
        int g = Integer.MAX_VALUE;
        if(g%2!=0){g--;}
        LinkedList<Integer> v1 = new LinkedList<Integer>();
        LinkedList<Integer> v2 = new LinkedList<Integer>();

        String precision_s = "0.4";
        for(int i = 1;i<line.length();i++){
            precision_s+="9999";
        }

        double division_diff = 0.33;
        double precision_d = Double.parseDouble(precision_s);
        

        int g1 = (int)(g*precision_d);
        int g2 = g-g1;

        int n_v1=(int)(n*division_diff);
        int n_v2= n-n_v1;

        
        
        Random rnd = new Random(System.currentTimeMillis());
        for(int i=0;i<n_v1-1;i++){   
            int min_part_1 = (int)(g1/(n_v1-i));
            int a = rnd.nextInt(min_part_1+1+n)+min_part_1-n;
            if(a!=0){
                g1=g1-a;
                v1.add(a);
            }
        }
        v1.add(g1);
        for(int i=0;i<n_v2-1;i++){
            int min_part_2 = (int)(g2/(n_v2-i));
            int a = rnd.nextInt(min_part_2+1+n)+min_part_2-n;
            if(a!=0){
                g2=g2-a;
                v2.add(a);
            }

        }
        v2.add(g2);


        LinkedList<String> result = new LinkedList<String>();
        LinkedList<Integer> shuffle = new LinkedList<Integer>();

        shuffle.addAll(v1);
        shuffle.addAll(v2);

        for (Integer i1 : v1) {
            result.add("s1:"+i1);
        }
        for (Integer i2 : v2) {
            result.add("s2:"+i2);
        }
        
        Collections.shuffle(shuffle);
        for (Integer integer : shuffle) {
            System.out.println(integer);
        }


        Collections.shuffle(result);

        System.out.println("r");
        for (String ln : result) {
            System.out.println(ln);
        }


    }
}
