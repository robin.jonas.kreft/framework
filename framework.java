import java.io.BufferedReader;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.Random;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class framework {

    public static void main(String[] args) throws IOException, InterruptedException {

        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));
        String[] buffer_vals = new String[9];
        System.out.print("\033[H\033[2J");  
        System.out.print("\033[35m");
        System.out.println(" ___ ___    _   __  __ _____      _____  ___ _  __");
        System.out.println("| __| _ \\  /_\\ |  \\/  | __\\ \\    / / _ \\| _ \\ |/ /");
        System.out.println("| _||   / / _ \\| |\\/| | _| \\ \\/\\/ / (_) |   / ' <");
        System.out.println("|_| |_|_\\/_/ \\_\\_|  |_|___| \\_/\\_/ \\___/|_|_\\_|\\_\\\n\n");
        System.out.print("\033[39m");                   

        for(int s=0;s<9;s++){
            switch (s) {
                case 0:
                    System.out.print("Simulation [false/true]:\t");
                    buffer_vals[s] = rd.readLine();
                    break;
                case 1:
                    if(buffer_vals[0].equals("false")){
                        System.out.print("Wettkampf [false/true]:\t\t");
                        buffer_vals[s] = rd.readLine();
                    }
                    break;
                case 2:
                    System.out.print("Ausgaben [false/true]:\t\t");
                    buffer_vals[s] = rd.readLine();
                    break;
                case 3:
                    if(buffer_vals[0].equals("false")){
                        System.out.print("Startinstanz [Zahl]:\t\t");
                        buffer_vals[s] = rd.readLine();
                    }
                    break;
                case 4:
                    if(buffer_vals[0].equals("false")){
                        System.out.print("Inkrement [Zahl]:\t\t");
                        buffer_vals[s] = rd.readLine();
                    }
                    break;
                case 5:
                    System.out.print("Modus [Name]:\t\t\t");
                    buffer_vals[s] = rd.readLine();
                    break;
                case 6:
                    System.out.print("Heap Generator [Mb]:\t\t");
                    buffer_vals[s] = rd.readLine();
                    break;
                case 7:
                    System.out.print("Heap Solver [Mb]:\t\t");
                    buffer_vals[s] = rd.readLine();
                    break;
                case 8:
                    System.out.print("maximale Zeit [s]:\t\t");
                    buffer_vals[s] = rd.readLine();
                    break;
            
                default:
                    break;
            }
        }

        boolean comp = Boolean.parseBoolean(buffer_vals[0])?false:Boolean.parseBoolean(buffer_vals[1]);
        boolean sim = Boolean.parseBoolean(buffer_vals[0]);
        boolean debug = Boolean.parseBoolean(buffer_vals[2]);
        String mode = buffer_vals[5];
        int heap_g = Integer.parseInt(buffer_vals[7]);
        int heap_s = Integer.parseInt(buffer_vals[7]);
        int max_time = Integer.parseInt(buffer_vals[8]);
        input ip = Boolean.parseBoolean(buffer_vals[0])?new input(1,10):new input(Integer.parseInt(buffer_vals[3]), Integer.parseInt(buffer_vals[4]));


        FilenameFilter javafiles = new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.toLowerCase().endsWith(".java");
            }
        };

        FilenameFilter classes_to_delete = new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.toLowerCase().endsWith(".class");
            }
        };

        LinkedList<String> names = new LinkedList<String>();

        File path = new File("./generate");
        for(int i=0;i<2;i++){
            for (File fl : path.listFiles(javafiles)) {
                names.add(fl.getName().toLowerCase().split("\\.")[0]);
                ProcessBuilder pb = new ProcessBuilder("cmd.exe", "/c", "cd "+path.getCanonicalPath()+" && javac "+ fl.getName());
                pb.redirectErrorStream(true);
                Process p = pb.start();
                BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
                while(reader.readLine()!=null){
                    System.out.println(reader.readLine());
                };
                reader.close();
            }
            path = new File("./solve");
        }

        FilenameFilter classfiles = new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                for (String string : names) {
                    if(string.equals(name.toLowerCase().split("\\.")[0])){
                        return name.toLowerCase().endsWith(".class");
                    }
                }
                return false;
                
            }
        };

 
        LinkedList<String> gen_paths = new LinkedList<String>();
        LinkedList<String> solve_paths = new LinkedList<String>();


        path = new File("./generate");
        for (File gen : path.listFiles(classfiles)) {
            gen_paths.add(gen.getName().replaceFirst("[.][^.]+$", ""));
        }
        path = new File("./solve");
        for (File solve : path.listFiles(classfiles)) {
            solve_paths.add(solve.getName().replaceFirst("[.][^.]+$", ""));
        }

        String abs_path_g = new File("./generate").getCanonicalPath();
        String abs_path_s = new File("./solve").getCanonicalPath();

        int sim_gen = -1;
        int sim_solve = -1;
        if(sim){
            System.out.print("\033[6A\033[0J");  
            Scanner sim_input = new Scanner(System.in);
            for (String gen : gen_paths) {
                System.out.println(gen_paths.indexOf(gen)+": "+gen);
            }
            System.out.print("\nWelcher Generator? >> ");
            sim_gen= sim_input.nextInt();
            System.out.println("------------------------");
            for (String solve : solve_paths) {
                System.out.println(solve_paths.indexOf(solve)+": "+solve);
            }
            System.out.print("\nWelcher Solver? >> ");
            sim_solve= sim_input.nextInt();
        }

        String winner = "niemand";
        String timedata = "Instanz";
        for (String g : gen_paths) {
            for (String s : solve_paths) {
                timedata+="\t"+g+"/"+s;
            }
        }
        timedata+="\n";
        

        LinkedList<String> beaten = new LinkedList<String>();

        boolean ended = false;
        boolean upper_found = false;
        int upper_border = 0;
        boolean lower_found = false;
        int lower_border = 0;
        boolean cmp = true;
        int v;
        while(!ended){
            if(sim){
                v = ip.mult_input(upper_found, upper_border, lower_found, lower_border);
            }else{
                v = ip.gen_next_input();
            }
            if(comp){
                Thread.sleep(1000);
            }
            Thread.sleep(100);
            System.out.print("\033[H\033[2J");  
            System.out.flush();  

            System.out.println("\033[1;36mInstanz: "+v+"\033[0m");

            mainloop:{
                for (String gen : gen_paths) {
                    if(sim && gen_paths.indexOf(gen)!=sim_gen){
                        continue;
                    }
                    if(!sim && !cmp){
                        Thread.sleep(1000);
                    }
                    LinkedList<String> instance = new LinkedList<String>();

                    ProcessBuilder processbuild_g = new ProcessBuilder("cmd.exe", "/c", "cd "+abs_path_g+" && java -Xmx"+heap_g+"m -Xms"+heap_g/2+"m "+gen+" "+v);
                    processbuild_g.redirectErrorStream(true);

                    System.out.print("\nGenerator \""+gen+"\": "); 

                    long starttime_g = System.currentTimeMillis();
                    Process process_g = processbuild_g.start();

                    BufferedReader reader_g = new BufferedReader(new InputStreamReader(process_g.getInputStream()));
                    Stream<String> outputstream_g = reader_g.lines();
                    instance.addAll(outputstream_g.collect(Collectors.toList()));
                    reader_g.close();

                    process_g.waitFor();
                    process_g.destroyForcibly();
                    System.out.println(String.format("%.3f",(float)(System.currentTimeMillis()-starttime_g)/1000).replace(",", ".")+" s");
            
                    data_manage dm = new data_manage(instance, mode);
                    String solver_input = dm.solver_input();
                    String result = dm.result();

                    Files.write(Paths.get(abs_path_s+"\\solver_input"),solver_input.getBytes());

                    for (String solve : solve_paths) {
                        if(sim && solve_paths.indexOf(solve)!=sim_solve){
                            continue;
                        }
                        if(comp && (solve.equals(gen) || beaten.contains(solve))){
                            continue;
                        }
                        if(!sim && !cmp){
                            Thread.sleep(1000);
                        }
                        LinkedList<String> solution = new LinkedList<String>();

                        ProcessBuilder processbuild_s = new ProcessBuilder("cmd.exe", "/c", "cd "+abs_path_s+" && type solver_input | java -Xmx"+heap_s+"m -Xms"+heap_s/2+"m "+solve);
                        processbuild_s.redirectErrorStream(true);

                        System.out.print("\tSolver \""+solve+"\": 0.000 s");

                        long starttime_s = System.currentTimeMillis();
                        Process process_s = processbuild_s.start();

                        BufferedReader reader_s = new BufferedReader(new InputStreamReader(process_s.getInputStream()));
                        boolean timeout = true;

                        float t_show;
                        String time_str;
                        int shift=7;
                        Random rnd = new Random(System.currentTimeMillis());
                        while(System.currentTimeMillis()-starttime_s<max_time*1000+2){
                            if((System.currentTimeMillis()-starttime_s)%rnd.nextInt(75, 100)==0){
                                t_show = (float)(System.currentTimeMillis()-starttime_s)/1000;
                                time_str = String.format("%.3f",t_show).replace(",", ".");
                                System.out.print("\033["+shift+"D");
                                System.out.print("\033[0K");
                                System.out.print(time_str+" s");
                                shift = 6+time_str.split("\\.")[0].length();
                            }


                            if(reader_s.ready()){
                                timeout=false;
                                break;
                            }
                        }
                        String solver_res="";
                        if(timeout){
                            process_s.destroyForcibly();
                            solver_res="Timeout!";
                            cmp=false;
                            System.out.print("\n");
                        }
                        if(!timeout){
                            Stream<String> outputstream_s = reader_s.lines();
                            solution.addAll(outputstream_s.collect(Collectors.toList()));
                            reader_s.close();

                            process_s.destroyForcibly();

                            System.out.print("\033["+shift+"D");
                            System.out.print("\033[0K");
                            System.out.println(String.format("%.3f",(float)(System.currentTimeMillis()-starttime_s)/1000).replace(",", ".")+" s");

                            solver_res = dm.solver_to_String(solution);
                            cmp = dm.compare();
                            if(!sim && !comp){
                                if(gen_paths.indexOf(gen)==0){
                                    timedata+=v;
                                }
                                timedata+="\t"+(cmp?String.format("%.3f",(float)(System.currentTimeMillis()-starttime_s)/1000):"");
                        
                                if(solve_paths.indexOf(solve)==solve_paths.size()-1 && gen_paths.indexOf(gen)==gen_paths.size()-1){
                                    timedata+="\n";
                                }
                            }
                        }

                        if(debug){
                            if(!cmp){
                                System.out.println("\t\033[30mSolver Fehler: "+solver_res+" \n\tGenerator Loesung: "+result+"\033[0m");
                            }else{
                                System.out.println("\t\033[30mSolver Loesung: "+solver_res+" \n\tGenerator Loesung: "+result+"\033[0m");
                            }
                            
                        }

                        if(!cmp){
                            if(comp){
                                if(timeout){
                                    System.out.println("\t\033[91mTimeout von "+solve); 
                                }else{
                                    System.out.println("\t\033[91mFalsche Loesung von "+solve);
                                }
                                System.out.println("\t\033[92mBester Wert: "+(v-ip.get_interval())+"\033[0m");
                                beaten.add(solve);
                                String msg = "\n\033[5mEnter zum fortfahren >>\033[0m ";
                                System.out.print(msg);
                                Scanner press_next = new Scanner(System.in);
                                String next = press_next.nextLine();
                                System.out.print("\033[3A");
                                System.out.print("\033[0J");
                                continue;
                            }
                            if(!sim){
                                Scanner error_handling = new Scanner(System.in);
                                if(timeout){
                                    System.out.print("\n\033[91mTimeout von "+solve+": Fortfahren? >>\033[0m ");
                                }else{
                                    System.out.print("\n\033[91mFehler von "+solve+": Fortfahren? >>\033[0m ");
                                }
                                int answ = error_handling.nextInt();
                                if(answ ==0){
                                    ended = true;
                                    break mainloop;
                                }
                                System.out.print("\033[2A");
                                System.out.print("\033[0J");
                                continue;
                            }
                            else{
                                upper_border = v;
                                upper_found = true;
                            }
                            break mainloop;
                        }
                        if(comp){
                            System.out.println("\t\033[92mInstanz "+v+" bestanden.\033[0m");
                        }
                        if(sim){
                            lower_border = v;
                            lower_found =true;
                        }
                        
                    }

                }
            }

            if((!comp && !sim) || comp && (beaten.size() >= solve_paths.size()-1)){
                if(comp){
                    for (String s : solve_paths) {
                        if(!beaten.contains(s)){
                            winner = s;
                        }
                    }

                    System.out.println("\033[95m\n"+winner+" hat gewonnen!\033[0m");

                    if(!winner.equals("niemand")){
                        Scanner next_round = new Scanner(System.in);
                        System.out.print("\nGrenze von "+winner+" simulieren? >> ");
                        int answ = next_round.nextInt();
                        if(answ ==0){
                            ended = true;
                            continue;
                        }
                        int selected_gen=-1;
                        LinkedList<String> other_gens = new LinkedList<String>();
                        for (String gen : gen_paths) {
                            if(!gen.equals(winner)){
                                other_gens.add(gen);
                            }
                        }
                        if(other_gens.size()==1){
                            selected_gen = gen_paths.indexOf(other_gens.getFirst());
                        }else{
                            System.out.println();
                            Scanner select_next_gen = new Scanner(System.in);
                            for (String gen : other_gens) {
                                System.out.println(gen_paths.indexOf(gen)+": "+gen);
                            }
                            System.out.print("\nWelcher Generator? >> ");
                            selected_gen= select_next_gen.nextInt();
                        }
                        sim = true;
                        comp = false;
                        sim_solve = solve_paths.indexOf(winner);
                        sim_gen = selected_gen;
                        ip = new input(1,10);
                        continue;
                    }

                }
                Scanner next_round = new Scanner(System.in);
                System.out.print("\nWeiter? >> ");
                int answ = next_round.nextInt();
                if(answ ==0){
                    ended = true;
                    if(!comp && !sim){
                        System.out.print("\033[H\033[2J");  
                        System.out.flush(); 
                        System.out.print("\nLaufzeiten speichern? >> ");
                        if(next_round.nextInt()==1){
                            if(Files.notExists(Paths.get("./runtime_data"))){
                                Files.createDirectory(Paths.get("./runtime_data"));
                            }
                            Date d = new Date();
                            SimpleDateFormat f = new SimpleDateFormat("dd-MM-yyyy_HH-mm-ss");
                            Files.write(Paths.get("./runtime_data/timedata_"+f.format(d)+".csv"),timedata.getBytes());
                        }
                    }
                }
            }
            if(sim){
                if(upper_found && lower_found && upper_border-lower_border<2){
                    System.out.println("\n\033[95mSolver "+solve_paths.get(sim_solve)+" lief mit Generator "+gen_paths.get(sim_gen)+" bis "+lower_border);
                    ended = true;
                    continue;
                }
            }
            
        }

        path = new File("./generate");
        for (File gen : path.listFiles(classes_to_delete)) {
            gen_paths.add(gen.getName().replaceFirst("[.][^.]+$", ""));
        }
        path = new File("./solve");
        for (File solve : path.listFiles(classes_to_delete)) {
            solve_paths.add(solve.getName().replaceFirst("[.][^.]+$", ""));
        }

        for (String gen : gen_paths) {
            ProcessBuilder processbuild_rem_g = new ProcessBuilder("cmd.exe", "/c", "cd "+abs_path_g+" && del "+gen+".class");
            processbuild_rem_g.redirectErrorStream(true);
            Process	remove_gen_classes = processbuild_rem_g.start();
            BufferedReader reader_rem_g = new BufferedReader(new InputStreamReader(remove_gen_classes.getInputStream()));
            while(reader_rem_g.readLine()!=null){
                // System.out.println(reader_rem_g.readLine());
            };
            reader_rem_g.close();
        }
        for (String solve : solve_paths) {
            ProcessBuilder processbuild_rem_s = new ProcessBuilder("cmd.exe", "/c", "cd "+abs_path_s+" && del "+solve+".class");
            processbuild_rem_s.redirectErrorStream(true);
            Process remove_solve_classes = processbuild_rem_s.start();
            BufferedReader reader_rem_s = new BufferedReader(new InputStreamReader(remove_solve_classes.getInputStream()));
            while(reader_rem_s.readLine()!=null){
                // System.out.println(reader_rem_s.readLine());
            };
            reader_rem_s.close();
        }
        


    }








    
}
