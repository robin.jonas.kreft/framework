import java.util.Arrays;
import java.util.Scanner;
import java.util.LinkedList;

class Bubblesort
{
	// Utility function to swap values at two indices in the array
	public static void swap(int[] arr, int i, int j)
	{
		int temp = arr[i];
		arr[i] = arr[j];
		arr[j] = temp;
	}

	// Function to perform bubble sort on a given array `arr[]`
	public static void bubbleSort(int[] arr)
	{
		// `n-1` passes where `n` is the array's length
		for (int k = 0; k < arr.length - 1; k++)
		{
			// last `k` items are already sorted, so the inner loop can
			// avoid looking at the last `k` items
			for (int i = 0; i < arr.length - 1 - k; i++)
			{
				if (arr[i] > arr[i + 1]) {
					swap(arr, i, i + 1);
				}
			}

			// the algorithm can be terminated if the inner loop
			// didn't do any swap
		}
	}

	public static void main(String[] args)
	{
		Scanner reader = new Scanner(System.in);
        String[] input = reader.nextLine().split(" ");
        reader.close();
        LinkedList<Integer> tosort = new LinkedList<Integer>();
        for (String s : input) {
            tosort.add(Integer.parseInt(s));
        };
		int arr[] = tosort.stream().mapToInt(i->i).toArray();

		bubbleSort(arr);

		// print the sorted array
		System.out.println(Arrays.toString(arr));
	}
}