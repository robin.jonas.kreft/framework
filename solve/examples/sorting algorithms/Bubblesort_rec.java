import java.util.Arrays;
import java.util.Scanner;

public class Bubblesort_rec {

    public static int[] intArr;
    public static int[] sort() {
        int k;
        for (int i = 0; i < intArr.length - 1; i++) {
            if (intArr[i] < intArr[i + 1]) {
                continue;
            }
            k = intArr[i];
            intArr[i] = intArr[i + 1];
            intArr[i + 1] = k;
            sort();
        }
        return intArr;
    }

    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        String[] input = reader.nextLine().split(" ");
        reader.close();
        intArr = new int[input.length];

        for(int i=0;i<intArr.length;i++){
            intArr[i]=Integer.parseInt(input[i]);
        }
        int[] arr = Bubblesort_rec.sort();
        System.out.println(Arrays.toString(arr));
    }
}
