import java.util.Collections;
import java.util.LinkedList;
import java.util.Scanner;

public class javasort {
    public static void main(String[] args) {
        
        Scanner reader = new Scanner(System.in);
        String[] input = reader.nextLine().split(" ");
        reader.close();
        LinkedList<Integer> tosort = new LinkedList<Integer>();
        for (String s : input) {
            tosort.add(Integer.parseInt(s));
        }
        Collections.sort(tosort);
        System.out.println(tosort.toString());


    }
}
