import java.util.Arrays;
import java.util.Scanner;
import java.util.LinkedList;

class InsertionSort
{
	// Function to perform insertion sort on `arr[]`
	public static void insertionSort(int[] arr)
	{
		// Start from the second element
		// (the element at index 0 is already sorted)
		for (int i = 1; i < arr.length; i++)
		{
			int value = arr[i];
			int j = i;

			// find index `j` within the sorted subset `arr[0…i-1]`
			// where element `arr[i]` belongs
			while (j > 0 && arr[j - 1] > value)
			{
				arr[j] = arr[j - 1];
				j--;
			}

			// note that the subarray `arr[j…i-1]` is shifted to
			// the right by one position, i.e., `arr[j+1…i]`

			arr[j] = value;
		}
	}

	public static void main(String[] args)
	{
		Scanner reader = new Scanner(System.in);
        String[] input = reader.nextLine().split(" ");
        reader.close();
        LinkedList<Integer> tosort = new LinkedList<Integer>();
        for (String s : input) {
            tosort.add(Integer.parseInt(s));
        };
		int arr[] = tosort.stream().mapToInt(i->i).toArray();
		insertionSort(arr);

		// print the sorted array
		System.out.println(Arrays.toString(arr));
	}
}