import java.util.Arrays;
import java.util.Scanner;

public class Quicksort_rec {

    public static int[] intArr;

    public static int[] sort(int l, int r) {
        int q;
        if (l < r) {
            q = partition(l, r);
            sort(l, q);
            sort(q + 1, r);
        }
        return intArr;
    }

    private static int partition(int l, int r) {
        int i, j, x = intArr[(l + r) / 2];
        i = l - 1;
        j = r + 1;

        do {
            i++;
        } while (intArr[i] < x);

        do {
            j--;
        } while (intArr[j] > x);

        if (i < j) {
            int k = intArr[i];
            intArr[i] = intArr[j];
            intArr[j] = k;
        } else {
            return j;
        }
        return -1;
    }

    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        String[] input = reader.nextLine().split(" ");
        reader.close();
        intArr = new int[input.length];

        for(int i=0;i<intArr.length;i++){
            intArr[i]=Integer.parseInt(input[i]);
        }
        int[] arr = sort(0, intArr.length - 1);
        System.out.println(Arrays.toString(arr));
    }
}
