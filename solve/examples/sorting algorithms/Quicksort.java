import java.util.Arrays;
import java.util.LinkedList;
import java.util.Scanner;

class QuickSort {

    static int partition(int arr[], int low, int high)
    {
        int pivot = arr[high];


        int i = (low - 1);
        for (int j = low; j <= high - 1; j++) {

            if (arr[j] <= pivot) {
                i++;

                int temp = arr[i];
                arr[i] = arr[j];
                arr[j] = temp;
            }
        }

        int temp = arr[i + 1];
        arr[i + 1] = arr[high];
        arr[high] = temp;

        return i + 1;
    }

    static void quickSortIterative(int arr[], int l, int h)
    {
        int[] stack = new int[h - l + 1];
        int top = -1;

        stack[++top] = l;
        stack[++top] = h;

        while (top >= 0) {

            h = stack[top--];
            l = stack[top--];

            int p = partition(arr, l, h);

            if (p - 1 > l) {
                stack[++top] = l;
                stack[++top] = p - 1;
            }

            if (p + 1 < h) {
                stack[++top] = p + 1;
                stack[++top] = h;
            }
        }
    }

    public static void main(String args[])
    {

        Scanner reader = new Scanner(System.in);
        String[] input = reader.nextLine().split(" ");
        reader.close();
        LinkedList<Integer> tosort = new LinkedList<Integer>();
        for (String s : input) {
            tosort.add(Integer.parseInt(s));
        }

        int arr[] = tosort.stream().mapToInt(i->i).toArray();
        int n = arr.length;


        quickSortIterative(arr, 0, n - 1);

        System.out.println(Arrays.toString(arr));
    }
}

