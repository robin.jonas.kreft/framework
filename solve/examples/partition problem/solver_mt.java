import java.util.Collections;
import java.util.LinkedList;
import java.util.Scanner;

public class solver_mt{

    public static void main(String[] args) throws InterruptedException {

        LinkedList<Integer> values = new LinkedList<Integer>();

        Scanner reader = new Scanner(System.in);
        String[] input = reader.nextLine().split(" ");
        reader.close();
        for (String string : input) {
            values.add(Integer.parseInt(string));
        }

        LinkedList<solver_thread> solver = new LinkedList<solver_thread>();
        LinkedList<Thread> threads = new LinkedList<Thread>();

        for(int i=0;i<20000;i++){
            int[] buffer = new int[values.size()];
            for(int j=0;j<values.size();j++){
                buffer[j]=values.get(j);
            }
            LinkedList<Integer> sl = new LinkedList<Integer>();
            for (int v : buffer) {
                sl.add(v);
            }
            solver_thread s = new solver_thread(sl);
            solver.add(s);
            Thread s_t = new Thread(s);
            threads.add(s_t);
            s_t.start();
        }

        long wtime = System.currentTimeMillis();
        while(System.currentTimeMillis()-wtime<25000){

        }


        solver_thread min_diff_s = null;
        int mindiff = Integer.MAX_VALUE;
        for (solver_thread s : solver) {
            int cdiff = s.getdiff();
            if(cdiff<mindiff){
                min_diff_s=s;
                mindiff=cdiff;
            }
        }

        LinkedList<Integer> bunch1 = min_diff_s.getList1();
        LinkedList<Integer> bunch2 = min_diff_s.getList2();


        for (Integer i1 : bunch1) {
            System.out.println("s1:"+i1);
        }

        for (Integer i2 : bunch2) {
            System.out.println("s2:"+i2);
        }


    }
}

class solver_thread implements Runnable{

    volatile LinkedList<Integer> bunch1;
    volatile LinkedList<Integer> bunch2;

    public solver_thread(LinkedList<Integer> values){
        Collections.shuffle(values);
        bunch1 = new LinkedList<Integer>();
        bunch1.addAll(values.subList(0, values.size()/2));
        bunch2 = new LinkedList<Integer>();
        bunch2.addAll(values.subList(values.size()/2,values.size()));
    }

    @Override
    public void run(){

        long ct = System.currentTimeMillis();
        while(System.currentTimeMillis()-ct<20000){
            int cdiff = calc_diff();

            if(cdiff>0){
                int max =0;
                int indx = -1;
                for (Integer integer : bunch1) {
                    if(integer>max && integer<Math.abs(cdiff)){
                        max = integer;
                        indx=bunch1.indexOf(integer);
                    }
                }
                if(indx<0){
                    break;
                }
                bunch2.add(bunch1.remove(indx));
            }
            if(cdiff<0){
                int max =0;
                int indx = -1;
                for (Integer integer : bunch2) {
                    if(integer>max && integer<Math.abs(cdiff)){
                        max = integer;
                        indx=bunch2.indexOf(integer);
                    }
                }
                if(indx<0){
                    break;
                }
                bunch1.add(bunch2.remove(indx));
            }

        }

    }


    private int calc_diff(){
        int sum1=0;
        int sum2=0;

        for (Integer i : bunch1) {
            sum1+=i;
        }
        for (Integer i : bunch2) {
            sum2+=i;
        }

        return sum1-sum2;
    }

    public LinkedList<Integer> getList1(){
        return bunch1;
    }
    public LinkedList<Integer> getList2(){
        return bunch2;
    }
    public int getdiff(){
        return Math.abs(calc_diff());
    }


}