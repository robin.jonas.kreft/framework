import java.util.Collections;
import java.util.LinkedList;
import java.util.Random;
import java.util.Scanner;

public class solver{

    public static void main(String[] args) {

        String gen_diff = "";
        LinkedList<Integer> values = new LinkedList<Integer>();

        Scanner reader = new Scanner(System.in);
        String[] input = reader.nextLine().split(" ");
        reader.close();
        for (String string : input) {
            values.add(Integer.parseInt(string));
        }


        LinkedList<Integer> bunch1 = new LinkedList<Integer>();
        LinkedList<Integer> bunch2 = new LinkedList<Integer>();

        Collections.sort(values);

        int c=1;
        int b1 = 0;
        int b2 = 0;
        for (Integer integer : values) {
            if(c%2==0){
                bunch1.add(integer);
                b1+=integer;
                c++;
            }
            else{
                bunch2.add(integer);
                b2+=integer;
                c++;
            }
            
        }

        long time = System.currentTimeMillis();
        Random rnd = new Random(System.currentTimeMillis());
        while(System.currentTimeMillis()<time+30000 && b1!=b2){
            LinkedList<Integer> b1_copy= new LinkedList<Integer>(bunch1);
            LinkedList<Integer> b2_copy= new LinkedList<Integer>(bunch2);
            long l_time = System.currentTimeMillis(); 
            int diff = (int)Math.abs((double)b1-b2);
            int old_diff = diff;
            while(System.currentTimeMillis()<l_time+1000 && b1!=b2){

                for(int i = 0; i<(int)(bunch1.size()+bunch2.size())/8;i++){
                    int ind_1 = rnd.nextInt(bunch1.size());
                    int ind_2 = rnd.nextInt(bunch2.size());

                    bunch2.add(bunch1.remove(ind_1));
                    bunch1.add(bunch2.remove(ind_2));
                }

                if(b1>b2){
                    int[] index={0,b1+b2};
                    for (Integer el : bunch1) {
                        int d = (int)Math.abs(((double)b1-el)-((double)b2+el));
                        if(d<index[1]){
                            index[0]=bunch1.indexOf(el);
                            index[1]=d;
                        }
                    }
                    if(index[1]<diff){
                        bunch2.add(bunch1.remove(index[0]));
                    }
                }
                else if(b2>b1){
                    int[] index={0,b1+b2};
                    for (Integer el : bunch2) {
                        int d = (int)Math.abs(((double)b2-el)-((double)b1+el));
                        if(d<index[1]){
                            index[0]=bunch2.indexOf(el);
                            index[1]=d;
                        }
                    }
                    if(index[1]<diff){
                        bunch1.add(bunch2.remove(index[0])); 
                    }
                    
                }

                b1=0;
                b2=0;
                for (Integer i1 : bunch1) {
                    b1+=i1; 
                }
                for (Integer i2 : bunch2) {
                    b2+=i2; 
                }

            }

            if(diff>old_diff){
                bunch1.removeAll(bunch1);
                Collections.copy(bunch1, b1_copy);
                bunch2.removeAll(bunch2);
                Collections.copy(bunch2, b2_copy);
            }

        }



        for (Integer i1 : bunch1) {
            System.out.println("s1:"+i1);
        }

        for (Integer i2 : bunch2) {
            System.out.println("s2:"+i2);
        }


    }
}