import java.util.LinkedList;

public class data_manage {

    problem pr;

    String result, solver_in, solver_res;
    boolean err_flag = false;

    public data_manage(LinkedList<String> output_g, String name){

        pr = new problem(name);

        String[] buffer_str = output_g.toString().split("r");
        solver_in = buffer_str[0].replace("[","").replace("]", "").replace(",", "").replace("\n", "");
        result = buffer_str[1].replace("[","").replace("]", "").replace(",", "").replace("\n", "");
        result=result.trim();
        result = pr.m(result);

    }

    public String solver_to_String(LinkedList<String> output_s){

        if(output_s.toString().contains("Exception")){
            err_flag = true;
            if(output_s.toString().contains("OutOfMemoryError")){
                return "Fehler: Heap Speicher voll!";
            }
            if(output_s.toString().contains("StackOverflow")){
                return "Fehler: Stack überfüllt!";
            }

        } 

        solver_res = output_s.toString().replace("[","").replace("]", "").replace(",", "").replace("\n", "");
        solver_res = pr.m(solver_res);
        return solver_res;
    }
    
    public String result(){
        return result;
    }

    public String solver_input(){
        return solver_in;
    }

    public boolean compare(){
        if(err_flag){
            err_flag = false;
            return false;
        }
        return pr.c(result.split(" "),solver_res.split(" "));
    }

}
