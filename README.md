## FRAMEWORK
Framework ist ein Tool zu Testen und Vergleichen von Java Algorithmen.
### Installation
Die Installation erfolgt durch klonen dieses Repositories mit `git clone` und anschließendes Kompilieren des framworks mit `javac framework.java`.

------------

### Nutzung
Vor der Ausführung müssen .java Dateien in den Ordnern *solve* und *generate* vorhanden sein. Zu Beginn können die Beispielprogramme unter *\examples* verwendet werden.

Um Dateien ablegen zu können, die nicht verwendet werden sollen, kann ein beliebiger Ordner erstellt und die Dateien dort gespeichert werden. Die Ordnerstruktur sollte dann so aussehen:

```
    framework
    +---generate
    |      \---unused
    \---solve
           \---unused
```

Zum Strarten des Frameworks wird `java framework` genutzt.

------------

### Hinzufügen von Use-Cases
Die Logik für alle Probleme, die das Framework als Kontext nutzen kann, befindet sich in *problem.java*. Neue Probleme können dort hinzugefügt werden. Details dafür können in den Codeausschnitten gefunden werden.

------------

### Ausgaben
Wenn Laufzeiten gespeichert werden, erstellt das Framework einen neuen Ordner *runtime_data* und speichert dort die Ergebnisse als .csv Dateien mit dem Erstelldatum+Zeit als Dateinamen.
